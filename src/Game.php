<?php

namespace Bowling;

Class Game
{
    private $rounds;
    private $scores = [];

    /**
     * Game constructor.
     *
     * @param int $rounds
     */
    function __construct(int $rounds)
    {
        $this->rounds = $rounds;
    }

    /**
     * Score per shoot
     *
     * @param int $score
     */
    public function addScoreAfterShoot(int $score)
    {
        $this->scores[] = $score;
    }

    /**
     * @return int
     */
    public function getTotalScore(): int
    {
        $totalScore = 0;
        foreach ($this->scores as $score) {
            $totalScore += $score;
        }
        return $totalScore;
    }
}