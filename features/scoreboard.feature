Feature:
ScoreBoard showing the results of our bowling game


  Scenario: worst game
    Given I play a 1 round Bowling game
    When I bowl a game 0 0 0 0 0 0 0 0 0 0
    Then the game result will be 0

  Scenario: worst game
    Given I play a 1 round Bowling game
    When I bowl a game 0 0 1 0 0 0 0 0 0 0
    Then the game result will be 1

  Scenario: worst game
    Given I play a 1 round Bowling game
    When I bowl a game 0 0 1 0 0 5 0 0 0 0
    Then the game result will be 6